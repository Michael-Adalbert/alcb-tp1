#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

//blbla

#define LICENCE "GPL"
#define AUTEUR "Michael Adalbert michael.adalbert@univ-tlse3.fr"
#define DESCRIPTION "Exemple de module Master CAMSI"
#define DEVICE "Os à moelle"

char *chaine="World";
module_param(chaine, charp, S_IRUGO);

static int hello_init(void){
 printk(KERN_ALERT "Hello %s!\n", chaine);
 return 0;
}
static void hello_cleanup(void){
 printk(KERN_ALERT "Goodbye %s\n", chaine);
}

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);

module_init(hello_init);
module_exit(hello_cleanup);
